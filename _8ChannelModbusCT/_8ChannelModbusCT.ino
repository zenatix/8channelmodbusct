//Firmware version : 1.0     - 8ChannelModbusCT

#include <SimpleModbusSlave_DUE.h>
#include <EEPROM.h>
#include <Timer.h>
#include <OneWire.h>
#include <DallasTemperature.h>

//b17 think valley building first floor sector 32 gurgaon
//122001

//fuse bits   low:FF   ,high: DA   ,Ext: 05
// DS18B20 pin
#define MODBUS_ID_LOC 1
#define MODBUS_CONFIG_LOC 4
#define MODBUS_OK  1
#define MODBUS_DEFAULT_ID 1


#define HIGH_         (1)
#define LOW_          (0) 
 
#define   CT1         (A0)            // CT Mappings for ADC channels
#define   CT2         (A1)            
#define   CT3         (A2)      
#define   CT4         (A3)
#define   CT5         (A4)
#define   CT6         (A5)
#define   CT7         (A6)
#define   CT8         (A7)

#define   TX_EN       (2)             // Modbus chip enable pin

//////////////// registers of your slave ///////////////////
enum 
{     
  // just add or remove registers and your good to go...
  // The first register starts at address 0
  MB_0_R,     
  MB_1_R,
  MB_2_R,
  MB_3_R,        
  MB_4_R,     
  MB_5_R,
  MB_6_R,
  MB_7_R,        
  MB_8_R,         
  MB_9_T,
  MB_10_RE,
  MB_11_L,
  BATTERY,        //address 12
  MB_13_NOTUSED,
  MB_14_NOTUSED,
  MB_15_NOTUSED,
  MB_16_REBOOTCNT,
  MB_17_MODBUSID,
  MB_18_THRESHOLD,
  MB_19_HEARTBEAT,
  MB_20_CSA,/*CSA-> Current Sensor Analog Value*/
  MB_21_CSA,
  MB_22_CSA,
  MB_23_CSA,
  MB_24_PSA,
  MB_25_PSA,
  MB_26_CSA,
  MB_27_CSA,
  MB_28_CSA,
  MB_29_NOTUSED,
  MB_30_CSD,
  MB_31_CSD,/*CSD-> Current Sensor Digital Value*/
  MB_32_CSD,
  MB_33_CSD,
  MB_34_CSD,
  MB_35_CSD,
  MB_36_CSD,
  MB_37_CSD,
  MB_38_CSD,
  MB_39_TEMPRST,
  MB_40_TEMP_0_ADD,
  MB_41_TEMP_0_ADD,
  MB_42_TEMP_0_ADD,
  MB_43_TEMP_0_ADD,
  MB_44_TEMP_0_VAL,
  MB_40_TEMP_1_ADD,
  MB_41_TEMP_1_ADD,
  MB_42_TEMP_1_ADD,
  MB_43_TEMP_1_ADD,
  MB_44_TEMP_1_VAL,
  MB_40_TEMP_2_ADD,
  MB_41_TEMP_2_ADD,
  MB_42_TEMP_2_ADD,
  MB_43_TEMP_2_ADD,
  MB_44_TEMP_2_VAL,
  HOLDING_REGS_SIZE // leave this one
};

unsigned int holdingRegs[HOLDING_REGS_SIZE];      // Holding registers array
int reboot_val;                                   // Reboot count
static unsigned int heartbeatCount = 0;           // Heartbeat count for detecting device status
static unsigned char modbusId=0;                  // Modbus ID variable to hold the MODBUS ID  
float R1=100.0 , R2 = 2.0;                        // Values in kilo-ohm
int bat_mean[30];                                 // hold the value of bat every sec 
/*Timer instance */                                
Timer tm;                                         // Timer object for creating event  


void HeartBeatEvent(void){                        // This event is called every second
  heartbeatCount++;                               // Increment this variable to indicate the device is alive  
  heartbeatCount&=0xFFFF;                         // Unmask the usable bit 
  holdingRegs[MB_19_HEARTBEAT]=heartbeatCount;    // send this value to modbus register
}

void UpdateRebootCount(void){                     
  reboot_val = (int)EEPROM.read(10);              // Get old reboot value
  if (reboot_val==0xFF){                          // If first time or memory full condition occures then
    reboot_val = 0;                               // reset the counter value
  }
  else{
    reboot_val+=1;                                // inc by one
  }
  EEPROM.write(10,reboot_val);                    // Save the value in EEPROM
  //EEPROM.commit();
  holdingRegs[MB_16_REBOOTCNT] = reboot_val;      // Update the corresponding Holding register

}

void SetModbusId(unsigned char id){               // Function for setting the modbus ID
  EEPROM.write(MODBUS_ID_LOC,id);                 // Write this ID to eeprom
  modbusId = id;                                  // save this new modbus ID to the modbus variable
  modbus_update_comms(9600,modbusId);             // Update the modbus ID for modbus communication  
}

void GetModbusId(void){                           // Function for Reading the modbus ID
  unsigned char temp;                             
  temp =EEPROM.read(MODBUS_CONFIG_LOC);           // Read the saved configuration of the MODBUS 
  if(temp==MODBUS_OK){                            // If the modbus is saved in the EEPROM
     modbusId=EEPROM.read(MODBUS_ID_LOC);         // Then read from the eeprom
     holdingRegs[MB_17_MODBUSID] = modbusId;      // display this modbus ID to the MODBUS register
  }           
  else{                                             // Or it means default ID has to be set
    EEPROM.write(MODBUS_ID_LOC,MODBUS_DEFAULT_ID);  // Write the default ID to the EEPROM 
    EEPROM.write(MODBUS_CONFIG_LOC,MODBUS_OK);      // Set the configuration to the EEPROM location 
    holdingRegs[MB_17_MODBUSID] = modbusId;         // display this modbus ID to the MODBUS register
  }
}



void setup(){
  memset(holdingRegs,0,HOLDING_REGS_SIZE);          // set all registers to ones
  UpdateRebootCount();
  tm.every(1000,HeartBeatEvent);                    // Configure the timer for heartbeat counter
  GetModbusId();                                    // Get the stored modbus id
//  Serial.print(modbusId,DEC);
  modbus_configure(&Serial, 9600,MODBUS_DEFAULT_ID,TX_EN,HOLDING_REGS_SIZE, holdingRegs); // parameters(SerialPort,baudrate, ID, tx_en,Holding_reg_size,Holding_reg_addr)
  modbus_update_comms(9600,modbusId);               // Update the modbus ID
}

unsigned int ReadCT(uint8_t channel){             // Function that reads and returns the Load current by its INPUT CT
  int Vin = 0,temp = 0;                           
  float IL = 0.0;                                 // Load current variable  
  if((channel==CT7)||(channel==CT8)){             // IF LOW current channel is selected
   analogReference(INTERNAL);                     // reference volt 1.1 volt
   for(int i=0;i<50;i++){                         // Reading Peak for 50 seconds
    temp = analogRead(channel);                   
    if(temp>Vin) Vin=temp ; 
    delay(1);
  }                                  
   IL = ((Vin*1.5584)-2);                         // formulate the Load current
   if((unsigned int)IL>60000) IL = 0; 
   return IL;                                     // returnt the IL
  }
  else{                                           // else for other channels
    analogReference(DEFAULT);                     // set the reference to the 5 volt
   for(int i=0;i<50;i++){                         // Reading Peak
    temp = analogRead(channel);   
    if(temp>Vin) Vin=temp ;
    delay(1);
  }                                  
   IL = ((Vin*27.574)+150);                           // Formulate the voltage
   if(IL<=400) IL = 0;                             // Minimum 100 amp                 
   return IL;                                     // Return the load current for higher 
  }
}

void loop(){
  if(modbusId!=holdingRegs[MB_17_MODBUSID]){      // If the Id is different
    SetModbusId(holdingRegs[MB_17_MODBUSID]);     // Set this new ID 
  }
  holdingRegs[MB_20_CSA] = ReadCT(CT1);
  holdingRegs[MB_21_CSA] = ReadCT(CT2);
  holdingRegs[MB_22_CSA] = ReadCT(CT3);
  holdingRegs[MB_23_CSA] = ReadCT(CT4);           // Read all the CTs one by one
  holdingRegs[MB_24_PSA] = ReadCT(CT5);
  holdingRegs[MB_25_PSA] = ReadCT(CT6);
  holdingRegs[MB_26_CSA] = ReadCT(CT7);
  holdingRegs[MB_27_CSA] = ReadCT(CT8);
  tm.update();                                    // process relay if modbus holding register is modified 
}

void serialEvent() {
   modbus_update_new();
}


